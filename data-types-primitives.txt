default int - i32
==
let x = 5;
// same as:
let y = 5i32;
// or
let z: i32;
z = 5;
// or
let a: i32 = 5;
println!("x:{}, y:{}, z:{}, a:{}", x, y, z, a)


