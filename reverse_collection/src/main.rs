fn main() {
    // array
    println!("Array");
    let ar: [i64; 3] = [1,2,4];
    let r = reverseArray(ar);
    for i in 0..r.len() {
        println!("array el:{}", r[i]);
    }

    // vector
    println!("\nVector");
    let v: Vec<i64> = vec![1,2,4];
    let r = reverseVector(v);
    for i in 0..r.len() {
        println!("vector el:{}", r[i]);
    }
}

fn reverseArray(v: [i64; 3]) -> [i64; 3] {
    let mut m = v;
    m.reverse();
    return m;
}

fn reverseVector(v: Vec<i64>) -> Vec<i64> {
    let mut m = v;
    m.reverse();
    return m;
}
