fn main() {
    let col = [1,2,3,5,8,11];
    let folded = my_fold(&col);
    println!("folded: {}", folded);
}

fn my_fold(c: &[i32]) -> i32 {
    let res = c.iter().fold(0, |x,y| x+y);
    return res;
}