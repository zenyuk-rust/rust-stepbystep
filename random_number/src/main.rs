mod random;

use std::time;
use std::thread;



/// Random number generator is used to exercise numerical keyboard typing
fn main() {
    let mut r: u16;
    for _ in 0..12 {
        r = random::random();
        r = r % 1000;
        println!("{}", r);
        thread::sleep(time::Duration::from_secs(5));
    }
}

