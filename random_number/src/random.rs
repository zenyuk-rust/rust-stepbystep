use rand::Rng;

pub fn random() -> u16 {
    return rand::thread_rng().gen();
}